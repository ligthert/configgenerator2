from jinja2 import Template
import yaml
import math

# Utility functions
def compileList(items):
  content = ""
  for item in items:
    content = content + "\""+item+"\"," 
  return content[:-1]

# Config stuff
def cfgPatches(faction):
  f = open("templates/cfgPatches.tmpl",'r')
  template = Template(f.read())
  print(template.render(faction=faction['class']))


def cfgFactionClasses(faction):
  f = open("templates/cfgFactionClasses.tmpl","r")
  template = Template(f.read())
  print(template.render(faction=faction['class'],name=faction['name'],prepend=faction['prepend'],side=faction['side']))

def cfgVehicleClasses(faction):
  f = open("templates/cfgVehicleClasses.tmpl","r")
  template = Template(f.read())
  print(template.render(faction=faction['class']))

def cfgVehicles_open():
  f = open("templates/cfgVehicles_open.tmpl",'r')
  print(f.read())

def cfgVehicles_close():
  f = open("templates/cfgVehicles_close.tmpl",'r')
  print(f.read())

def cfgWeapons_open():
  f = open("templates/cfgWeapons_open.tmpl",'r')
  print(f.read())

def cfgWeapons_close():
  f = open("templates/cfgWeapons_close.tmpl",'r')
  print(f.read())

def render_soldier_deps(faction):
  f = open("templates/dependency.tmpl",'r')
  template = Template(f.read())
  print(template.render(dependency=faction['soldier_base']))

def render_soldiers(faction):
  f = open("templates/soldier.tmpl",'r')
  template = Template(f.read())
  
  soldiers = yaml.load( open("infantry.yaml",'r') )

  for soldier in soldiers:
  
    # Backpacks
    try:
      soldier_backpack = soldiers[soldier]['backpack']
    except KeyError:
      soldier_backpack = faction['soldier_backpack']

    # Icon
    try:
      soldier_icon=soldiers[soldier]['icon']
    except KeyError:
      soldier_icon=''    

    # isMedic
    try:
      soldier_isMedic=soldiers[soldier]['isMedic']
    except KeyError:
      soldier_isMedic=0

    # isEngineer
    try:
      soldier_isEngineer=soldiers[soldier]['isEngineer']
    except KeyError:
      soldier_isEngineer=0
    
    # isExplosive
    try:
      soldier_isExplosive=soldiers[soldier]['isExplosive']
    except KeyError:
      soldier_isExplosive=0
    
    # canHideBodies
    try:
      soldier_canHideBodies=soldiers[soldier]['canHideBodies']
    except KeyError:
      soldier_canHideBodies=0
    
    # items + addItems
    try:
      items = faction['items'] + soldiers[soldier]['add_items']
    except KeyError:
      try:
        items = soldiers[soldier]['items']
      except KeyError:
        items = faction['items']

    # weapons + add_weapons
    try:
      weapons = faction['weapons'] + soldiers[soldier]['add_weapons']
    except KeyError:
      try:
        weapons = soldiers[soldier]['weapons']
      except KeyError:
        weapons = faction['weapons']

    # magazines + add_magazines
    try:
      magazines = faction['magazines'] + soldiers[soldier]['add_magazines']
    except KeyError:
      try:
        magazines = soldiers[soldier]['magazines']
      except KeyError:
        magazines = faction['magazines']

    
    print( template.render(
          faction=faction['class'], 
          author=faction['author'],
          side=faction['side'],
          soldier_base=faction['soldier_base'], 
          soldier_model=faction['soldier_model'],
          soldier_type=soldier,
          soldier_name=soldiers[soldier]['name'],
          soldier_icon=soldier_icon,
          soldier_backpack=soldier_backpack,
          soldier_isMedic=soldier_isMedic,
          soldier_isEngineer=soldier_isEngineer,
          items=compileList(items), 
          weapons=compileList(weapons), 
          magazines=compileList(magazines) 
          ) )

def render_backpacks(faction):

  # Print the first empty backpack since we'll be inheriting of it.
  f = open("templates/backpack_empty.tmpl",'r')
  template = Template(f.read())

  print( template.render(
          faction=faction['class'], 
          author=faction['author'],
          prepend=faction['prepend'],
          backpack_base=faction['backpack_base'],
          backpack_name=faction['backpack_name']
        ) )

  # Print the rest of the backpacks
  f = open("templates/backpack.tmpl",'r')
  template = Template(f.read())
  
  backpacks = yaml.load( open("infantry_backpacks.yaml",'r') )

  for backpack in backpacks:
    try:
      if len(backpacks[backpack]['magazines']) >= 0:
        magazines = []
        for mag in backpacks[backpack]['magazines']:
          tmp_mag = mag.split(':')
          magazines.append({ 'class':tmp_mag[0], 'count':tmp_mag[1] })
    except KeyError:
      magazines = ''
    
    try:
      if len(backpacks[backpack]['items']) >= 0:
        items = []
        for mag in backpacks[backpack]['items']:
          tmp_item = mag.split(':')
          items.append({ 'class':tmp_item[0], 'count':tmp_item[1] })
    except KeyError:
      items = ''

    print( template.render(
          faction=faction['class'],
          author=faction['author'],
          prepend=faction['prepend'],
          backpack_type=backpack,
          backpack_name=faction['backpack_name'],
          magazines=magazines,
          items=items
          ))

def render_flags(faction):
  f = open("templates/flags.tmpl",'r')
  template = Template(f.read())

  flags = []
  try:
    for flag in faction['flags']:
      if len(flag) >= 0:
        flag_parts = flag.split(":")
        flags.append( {'class':flag_parts[0], 'file':flag_parts[1], 'name':flag_parts[2] } )
  except KeyError:
    flags = ''

  print( template.render(
        faction=faction['class'],
        author=faction['author'],
        prepend=faction['prepend'],
        flags=flags
        ))

def cfgGroups_open(faction):
  f = open("templates/cfgGroups_open.tmpl",'r')
  template = Template(f.read())

  if faction['side'] == 0:
    side = "EAST"
    side_l = "o"
  if faction['side'] == 1:
    side = "WEST"
    side_l = "b"
  if faction['side'] == 2:
    side = "Indep"
    side_l = "n"
  if faction['side'] == 3:
    side = "CIVILIAN"
    side_l = ""

  print( template.render(faction=faction['class'],side=side,prepend=faction['prepend'],faction_name=faction['name'],side_l=side_l) )

def cfgGroups(faction,gtype):
  f = open("templates/cfgGroups.tmpl",'r')
  template = Template(f.read())

  # Load the group yaml
  groups = yaml.load(open(gtype+"_groups.yaml",'r'))

  # Help the icon for 3den
  if faction['side'] == 0:
    side_l = "o"
  if faction['side'] == 1:
    side_l = "b"
  if faction['side'] == 2:
    side_l = "n"
  if faction['side'] == 3:
    side_l = ""

  if gtype == "Infantry":
      gtype_n = "inf"
  if gtype == "Motorized":
      gtype_n = "motor_inf"
  if gtype == "Mechanized":
      gtype_n = "mech_inf"
  if gtype == "Air":
      gtype_n = "plane"
  if gtype == "Armor":
      gtype_n = "armor"

  # Define rank
  rank = ["PRIVATE","CORPORAL","SERGEANT","LIEUTENANT","CAPTAIN","MAJOR","COLONEL"]

  # Cycle through it all
  tmpl_groups = []

  for group in groups:
    grpCount = 0
    grpUnits = []
    grpRank = groups[group]['rank']
    x = 0
    y = 0
    for composition in groups[group]['units']:
      # Determine the pos
      if grpCount == 0:
        x = 0
        y = 0
      else:
        if grpCount%2 == 0:
          x =  5 * math.floor(grpCount/2)
          y = -5 * math.floor(grpCount/2)
        else:
          x = -5 * math.floor(grpCount/2 + grpCount%2)
          y = -5 * math.floor(grpCount/2 + grpCount%2)

      pos = str(x)+","+str(y)+",0"

      # Add a unit to the group
      grpUnits.append( { "count" : grpCount, "rank": rank[grpRank], "pos": pos, "class": composition } )

      # Preparing for the next round
      if grpRank != 0:
        grpRank = grpRank - 1
      grpCount = grpCount + 1

    tmpl_groups.append( { 'class' : group , 'name' : groups[group]['name'], 'units': grpUnits } )

  print( template.render(
        faction=faction['class'],
        side=faction['side'],
        groups=tmpl_groups,
        side_l=side_l,
        gtype=gtype,
        gtype_n=gtype_n
        ))


def cfgGroups_close(faction):
  f = open("templates/cfgGroups_close.tmpl",'r')
  print(f.read())


def cfgUnitInsignia(faction):
  f = open("templates/cfgUnitInsignia.tmpl",'r')
  template = Template(f.read())

  insignias = []
  for insignia in faction['insignia']:
    ins_split = insignia.split(':')
    insignias.append( { 'class': ins_split[0], 'name': ins_split[1], 'file': ins_split[2] } )

  print( template.render(
        faction = faction['class'],
        prepend = faction['prepend'],
        author = faction['author'],
        insignias = insignias
        ))


def render_gear(faction):
  f = open("templates/gear_camo.tmpl",'r')
  template = Template(f.read())
  print( template.render(
        faction = faction['class'],
        prepend = faction['prepend'],
        author = faction['author'],
        camo_name = faction['camo_name'],
        soldier_model = faction['soldier_model']
        ))

  f = open("templates/gear_helmet.tmpl",'r')
  template = Template(f.read())
  print( template.render(
        faction = faction['class'],
        prepend = faction['prepend'],
        author = faction['author'],
        helmet_name = faction['helmet_name'],
        helmet_model = faction['helmet_model'],
        helmet_armor = faction['helmet_armor'],
        helmet_passthrough = faction['helmet_passthrough']
        ))

  f = open("templates/gear_vest.tmpl",'r')
  template = Template(f.read())
  print( template.render(
        faction = faction['class'],
        prepend = faction['prepend'],
        author = faction['author'],
        vest_name = faction['vest_name'],
        vest_model = faction['vest_model'],
        vest_class = faction['vest_class'],
        vest_mass = faction['vest_mass'],
        vest_armor = faction['vest_armor'],
        vest_passthrough = faction['vest_passthrough'],
        vest_chest_armor = faction['vest_chest_armor'],
        vest_chest_passthrough = faction['vest_chest_passthrough'],
        vest_diaphragm_armor = faction['vest_diaphragm_armor'],
        vest_diaphragm_passthrough = faction['vest_diaphragm_passthrough'],
        vest_abdomen_armor = faction['vest_abdomen_armor'],
        vest_abdomen_passthrough = faction['vest_abdomen_passthrough'],
        vest_pelvis_armor = faction['vest_pelvis_armor'],
        vest_pelvis_passthrough = faction['vest_pelvis_passthrough'],
        vest_body_armor = faction['vest_body_armor'],
        vest_body_passthrough = faction['vest_body_passthrough'],
        ))
  
