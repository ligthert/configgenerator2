#!/usr/bin/env python3

# Import required stuff
import yaml
import writeConfig as wc
import os.path

# Read config from the yaml file
faction = yaml.load(open('faction.yaml', 'r'))

# Obligatory cfgPatches
wc.cfgPatches(faction)

# Custom faction
wc.cfgFactionClasses(faction)

# Men vehicle class
wc.cfgVehicleClasses(faction)

# Open cfgVehicles
wc.cfgVehicles_open()

# Place dependencies
wc.render_soldier_deps(faction)

# Render soldiers
wc.render_soldiers(faction)

# Add backpacks
wc.render_backpacks(faction)

# Add flags (if any)
wc.render_flags(faction)

if os.path.isfile("custom_vehicle.config"):
  f = open("custom_vehicle.config",'r')
  print(f.read())

# Close cfgVehicles
wc.cfgVehicles_close()

# Open cfgWeapons
wc.cfgWeapons_open()

# Render gear for our soldiers
wc.render_gear(faction)

# Close cfgWeapons
wc.cfgWeapons_close()

# Groups: Opening
wc.cfgGroups_open(faction)

# Groups Infantry!
wc.cfgGroups(faction,"Infantry")
wc.cfgGroups(faction,"Motorized")
wc.cfgGroups(faction,"Mechanized")
wc.cfgGroups(faction,"Armor")
wc.cfgGroups(faction,"Air")

# Group: Closing
wc.cfgGroups_close(faction)

# Render insignias
wc.cfgUnitInsignia(faction)
